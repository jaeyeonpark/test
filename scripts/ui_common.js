﻿﻿///* Top GNB scroll --------------------------------------------*/
jQuery("document").ready(function($){
  var nav = $('.nav-container');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
    nav.addClass("small");
    } else {
    nav.removeClass("small");
    }
  });

  var h1 = $('.logo');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
     h1.addClass("small");
    } else {
     h1.removeClass("small");
    }
  });

  var div = $('.connect .disconnect');
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
     div.addClass("top");
    } else {
     div.removeClass("top");
    }
  });
  var $scroll = $('.x_scroll,.y_scroll');
  scrollTable($scroll);
});

$(window).resize(function() {
  // table
  var $scroll = $('.x_scroll,.y_scroll');
  for (var i = 0; i < $scroll.length; i++) {
    if(!$scroll.eq(i).parents().hasClass('popup')){
      scrollTable($scroll.eq(i));
      var $selectTarget = $('.select');
      selectCustom($selectTarget);
    }
  }
}); //$(window).resize(function()..

/* Layer Popup --------------------------------------------*/
function openPopup(e){
  $(e).addClass('on');
  popupPosition(e);
  // noneScroll();
}
// 레이어 팝업 닫기 
function closePopup(e){
  $(e).parents('.popup').removeClass('on');
  // noneScroll();
}
function closePopupOther(e){
  var $popup = $('.popup');
  if($popup.length){
    $('body').on('click',function(e){
      var $target = $(e.target);
      if($target.hasClass('popup')){
        $target.removeClass('on');
        // noneScroll();
      }
    });
  }
}
//레이어 팝업 위치 정하기
function popupPosition(e){
  for (var i = 0; i < e.length; i++) {
    var $pop_wrap = $(e).find('.pop_wrap').eq(i);
    $pop_wrap.css({
      'top' : Math.max(0, (($(window).height() - $pop_wrap.outerHeight()) / 2)) + "px",
      'left' : Math.max(0, (($(window).width() - $pop_wrap.outerWidth()) / 2)) + "px"
    });
  }
}
// 레이어 팝업 오픈시 스크롤 막기
 function noneScroll(){
   if($('.popup.on').length){
     if($('html').hasClass('ie8')){
       $('html,body').scrollTop(0);
     }
     window.setTimeout(function(){
       $('body').addClass('oh')
     }, 50);
   }else{
     $('body').removeClass('oh');
   }
 }

/* form -------------------------------------------*/
$(document).ready(function(){
  // 폼 커스텀
  var $selectTarget = $('.select');
  selectCustom($selectTarget);
  var $label = $('.radio label,.checkbox label,.switch label');
  matchLabel($label);
  matchName();
  checkboxAll();
  datetimeLoad();
});//$(document).ready(function()..
$(window).resize(function() {
  var $datetimeActive = $('.datetime.active,.date.active,.date_month.active,.time.active');
  datetimePosition($datetimeActive);
  var $on_popup = $('.popup.on');
  for (var i = 0; i < $on_popup.length; i++) {
    popupPosition($on_popup.eq(i));
  }
}); //$(window).resize(function()..
// datetime picker load
function datetimeLoad() {
  //Date+Time(HH:MM:SS) picker load
  if ($('.input.datetime').length > 0) {
    $('.input.datetime').datetimepicker({
      useCurrent: true,
      locale: 'ko',
      format: 'YYYY-MM-DD HH:mm',
      defaultDate: '2016-11-30 00:00',
      sideBySide: true,
      allowInputToggle: true,
      ignoreReadonly: true,
      //focusOnShow: false,
      //keepOpen: true,
      widgetParent: 'body'
    });
  }
  if ($('.input.date').length > 0) {
    $('.input.date').datetimepicker({
      useCurrent: true,
      locale: 'ko',
      format: 'YYYY-MM-DD',
      defaultDate: '2016-11-30',
      sideBySide: true,
      allowInputToggle: true,
      ignoreReadonly: true,
      //focusOnShow: false,
      //keepOpen: true,
      widgetParent: 'body'
    });
  }
  if ($('.input.date_month').length > 0) {
    $('.input.date_month').datetimepicker({
      useCurrent: true,
      locale: 'ko',
      format: 'YYYY-MM',
      defaultDate: '2016-11',
      sideBySide: true,
      allowInputToggle: true,
      ignoreReadonly: true,
      //focusOnShow: false,
      //keepOpen: true,
      widgetParent: 'body'
    });
  }
  if ($('.input.time').length > 0) {
    $('.input.time').datetimepicker({
      useCurrent: true,
      locale: 'ko',
      format: 'HH:mm',
      defaultDate: '2016-11-30 00:00',
      sideBySide: true,
      allowInputToggle: true,
      ignoreReadonly: true,
      //focusOnShow: false,
      //keepOpen: true,
      widgetParent: 'body'
    });
  }
  // datetime picker position
  var $dateclock = $('.input.datetime,.input.date,.input.date_month,.input.time');
  $dateclock.on('dp.show', function() {
    var $datetime = $('.datetime,.date,.date_month,.time');
    datetimePosition($(this));
    $datetime.removeClass('active');
    $(this).addClass('active');
    if ($(this).parents().hasClass('popup')) {
      var $datepicker = $('.bootstrap-datetimepicker-widget');
      $datepicker.css({
        zIndex: '1002'
      });
    }
  });
}
// datetime picker set position
function datetimePosition(e) {
  var $datepicker = $('.bootstrap-datetimepicker-widget');
  if ($datepicker.hasClass('bottom')) {
    var $top = e.offset().top + e.outerHeight();
    var $left = e.offset().left;
    $datepicker.css({
      top: $top + 'px',
      bottom: 'auto',
      left: $left + 'px'
    });
  } else if ($datepicker.hasClass('top')) {
    var $top = e.offset().top - $datepicker.outerHeight() - 3;
    var $left = e.offset().left;
    $datepicker.css({
      top: $top + 'px',
      bottom: 'auto',
      left: $left + 'px'
    });
  }
  if ($datepicker.hasClass('pull-right')) {
    var $left = e.offset().left - $datepicker.outerWidth() + e.outerWidth() - 6;
    $datepicker.css({
      left: $left + 'px'
    });
  }
}
// checkbox all toggle
function checkboxAll() {
  $('body').on('click', function(e) {
    var $target = $(e.target);
    var $nodeName = e.target.nodeName;
    if ($target.parent().hasClass('checkbox') && $nodeName == 'INPUT') {
      if ($target.hasClass('all')) {
        var $checkbox = $target.parents('.checkbox_all').find('.checkbox').find('input').not('all');
        if ($target.is(':checked')) {
          $checkbox.prop('checked', true);
        } else {
          $checkbox.prop('checked', false);
        }
      } else {
        var $checkbox_all = $target.parents('.checkbox_all').find('.checkbox').find('input.all');
        if (!$target.is(':checked')) {
          $checkbox_all.prop('checked', false);
        }
      }
    }
  });
}
//select custom
function selectCustom(e){
  for (var i = 0; i < e.length; i++) {
    var $value = e.eq(i).find('option:selected').text();
    // console.log($value);
    var $selectId = e.eq(i).find('select').attr('id');
    if(!$selectId == true){
      e.eq(i).find('select').attr('id','select_temp_id_'+i);
      $selectId = 'select_temp_id_'+i
    }
    var $label = '<label for="'+$selectId+'">'+ $value +'</label>';
    e.eq(i).find('label').remove();
    e.eq(i).append($label);
    e.eq(i).find('select').change(function(){
      $(this).find('option:selected').attr('selected','selected').siblings().removeAttr('selected');
      var $select_name = $(this).find('option:selected').text();
      $(this).siblings('label').text($select_name);
    });
  }
}
function inputChecked(e){
  for (var i = 0; i < e.length; i++) {
    var $label = e.eq(i).find('label');
    $label.append('<i class="before"></i>');
    inputLoad();
    e.eq(i).on('change',function(){
      inputLoad();
    });
  }
  function inputLoad(){
    for (var i = 0; i < e.length; i++) {
      if(e.eq(i).find('input').is(':checked')){
        e.eq(i).addClass('checked');
      }else{
        e.eq(i).removeClass('checked');
      }
    }
  }
}
function matchLabel(e){
  for (var i = 0; i < e.length; i++) {
    // set label input matching
    if(!e.eq(i).prev('input').attr('id')){
      e.eq(i).prev('input').attr('id','temp_id_'+(i+1));
    }
    e.eq(i).attr('for',e.eq(i).prev('input').attr('id'));
  }
}
function matchName(){
  // set radio name
  var $radioName = $('.radio_name');
  for (var i = 0; i < $radioName.length; i++) {
    $radio = $radioName.eq(i).find('.radio input');
    for (var j = 0; j < $radio.length; j++) {
      if(!$radio.eq(j).attr('name')){
        $radio.attr('name','temp_name_'+(i+1));
      }
    }
  };
}

//scroll Table
function scrollTable(e){
  // var $scroll = $('.x_scroll,.y_scroll');
  var $scroll = e;
  if(!$scroll) return;
    for (var i = 0; i < $scroll.length; i++) {
      var $table = $scroll.eq(i).find('table');
      var $table_width;
      var $td_array = new Array;
      var $col = $scroll.eq(i).find('colgroup').find('col');
      // set table layout auto
      $scroll.eq(i).addClass('load');
      if($scroll.eq(i).find('.thead,.tbody').length>0){
        // reset table
        var $thead_table = $scroll.eq(i).find('.thead').find('table');
        var $tbody = $scroll.eq(i).find('.tbody');
        $thead_table.unwrap();
        $thead_table.append($tbody.find('tbody'));
        $tbody.remove();
      }else{
        // fix col width
        if($scroll.eq(i).hasClass('x_scroll')){
          for (var j = 0; j < $col.length; j++) {
            if(!isNaN(parseInt($col.eq(j).attr('width')))){
              $col.eq(j).addClass('fix');
            }
          }
        }
      }
      // set col width before wrap
      if($scroll.eq(i).hasClass('x_scroll')){
        var $td = $table.find('tr:first-child').find('td');
        for (var j = 0; j < $td.length; j++) {
          var $td_width;
          if($col.eq(j).hasClass('fix')){
            $td_width = $col.eq(j).attr('width');
          }else{
            $td_width = Math.ceil($td.eq(j).outerWidth()) + 'px';
          }
          $col.eq(j).attr('width', $td_width);
          // get width
          $td_array[j] = parseInt($td_width);
        }
      }
      // wrap '.thead', '.tbody'
      if(!$scroll.eq(i).find('.thead,.tbody').length>0){
        var $thead = $scroll.eq(i).find('table').clone();
        var $tbody = $scroll.eq(i).find('table').clone();
        $scroll.eq(i).children('table').remove();
        $scroll.eq(i).append($thead).append($tbody);
        $thead.wrap('<div class="thead"></div>').find('tbody').remove();
        $tbody.wrap('<div class="tbody"></div>').find('thead').remove();
      }
      // get width
      $table_width = $td_array.reduce(function(a, b) { return a + b; }, 0);
      // set width
      if($scroll.eq(i).hasClass('x_scroll')){
        if($scroll.eq(i).outerWidth() >= $table_width){
          $scroll.eq(i).find('.thead,.tbody').css('width','100%');
        }else{
          $scroll.eq(i).find('.thead,.tbody').css('width',$table_width);
        }
      }
      // set table fixed
      $scroll.eq(i).removeClass('load');
    }
}
//scroll Table사용하는 곳에 graph_bar가 있을때 사용 

// function barAnimate(){
//   var $graph_bar = $('.graph_bar');
//   for (var i = 0; i < $graph_bar.length; i++) {
//     $bar = $graph_bar.eq(i).find('.bar');
//     $bar.addClass('bar_ani');
//   }
// }
